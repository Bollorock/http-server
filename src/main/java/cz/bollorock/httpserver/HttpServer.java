   /*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package cz.bollorock.httpserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class HttpServer {

    private final int port;
    private final ServerBootstrap b;
    private Channel ch;
    private final NioEventLoopGroup workerGroup;
    private final NioEventLoopGroup bossGroup;

    public HttpServer(int port, String webroot) throws IOException {
        this.port = port;

        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup();

        b = new ServerBootstrap();
        b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new HttpServerInitializer(webroot));
    }

    public void start() {
        if (ch != null) {
            throw new IllegalStateException("Server can be started only once.");
        }
        try {
            ch = b.bind(port).sync().channel();
            ch.closeFuture().addListener((ChannelFuture future) -> {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            });
            Logger.getLogger(HttpServer.class.getName()).log(Level.INFO, "Server started on port {0}", port);
        } catch (InterruptedException ex) {
            Logger.getLogger(HttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sync() throws InterruptedException {
        ch.closeFuture().sync();
    }

    public void stop() {
        ch.close();
        Logger.getLogger(HttpServer.class.getName()).log(Level.SEVERE, "Server shutdown");
    }

    public static void main(String[] args) throws Exception {
        setLoggingLevel(Level.WARNING);
        HttpServer httpServer = new HttpServer(80, "htdocs/");
        httpServer.start();
    }

    private static void setLoggingLevel(Level level) throws SecurityException {
        Logger.getLogger("").setLevel(level);
        Handler[] handlers = Logger.getLogger("").getHandlers();
        for (Handler handler : handlers) {
            handler.setLevel(level);
        }
    }
}

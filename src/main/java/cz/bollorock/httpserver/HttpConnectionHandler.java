/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import io.netty.handler.codec.http.LastHttpContent;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
@ChannelHandler.Sharable
public class HttpConnectionHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private final Map<HttpMethod, HttpRequestHandler> handlers;

    public HttpConnectionHandler(String webRoot) throws IOException {
        handlers = new ConcurrentHashMap<>();
        handlers.put(GET, new HttpGetHandler(webRoot));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
        if (!request.getDecoderResult().isSuccess()) {
            sendResponse(ctx, ErrorResponseFactory.sendError(
                    BAD_REQUEST,
                    request.getDecoderResult().toString()));
            return;
        }
        HttpRequestHandler handler = handlers.get(request.getMethod());
        if (handler == null) {
            sendResponse(ctx,
                    ErrorResponseFactory.sendError(
                            METHOD_NOT_ALLOWED,
                            "Method " + request.getMethod() + " isn't allowed."));
            return;
        }

        sendResponse(ctx, handler.handle(request));
    }

    private void sendResponse(ChannelHandlerContext ctx, DefaultFullHttpResponse response) {
        ctx.write(response);
        ChannelFuture future = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
        if (!HttpHeaders.isKeepAlive(response)) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface HttpRequestHandler {

    public DefaultFullHttpResponse handle(FullHttpRequest request);
}

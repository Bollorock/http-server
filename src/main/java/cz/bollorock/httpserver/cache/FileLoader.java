/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver.cache;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class FileLoader implements Loader<String, byte[]> {

    @Override
    public byte[] load(String key) throws Exception {
        return Files.readAllBytes(Paths.get(key, new String[0]));
    }

    @Override
    public long getLastModification(String key) throws Exception {
        return new File(key).lastModified();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver.cache;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class LoadingCache<K, V> {

    private final Map<K, SoftReference<CachedObject<V>>> cache;
    private final Loader<K, V> loader;
    private long defaultMaxAge = TimeUnit.MINUTES.toMillis(5);

    public LoadingCache(Loader<K, V> loader) {
        cache = new HashMap<>();
        this.loader = loader;
    }

    public V get(K key) throws Exception {
        return getWithMaxAge(key, defaultMaxAge, TimeUnit.MILLISECONDS);
    }

    public V getUpdated(K key) throws Exception {
        return getWithMaxAge(key, 0, TimeUnit.MILLISECONDS);
    }

    public V getWithMaxAge(K key, long timeValue, TimeUnit timeUnit) throws Exception {
        SoftReference<CachedObject<V>> ref = cache.get(key);
        if (ref == null) {
            CachedObject<V> co = new CachedObject<>(loader.load(key));
            cache.put(key, new SoftReference<>(co));
            return co.getData();
        } else {
            CachedObject<V> co = ref.get();
            if (co == null) {
                co = new CachedObject<>(loader.load(key));
                cache.put(key, new SoftReference<>(co));
                return co.getData();
            }
            long lastUpdate = loader.getLastModification(key);
            if ((timeValue >= 0 && co.getAge() > timeUnit.toMillis(timeValue))
                    || lastUpdate < 0 || lastUpdate > co.getLastUpdate()) {
                co.setData(loader.load(key));
            }
            return co.getData();
        }
    }

    public long getMaxAge() {
        return defaultMaxAge;
    }

    public void setMaxAge(long maxAge, TimeUnit timeUnit) {
        this.defaultMaxAge = timeUnit.toMillis(maxAge);
    }

    public void setMaxAge(long maxAge) {
        this.defaultMaxAge = maxAge;
    }

    private static class CachedObject<U> {

        private U data;
        private long lastUpdate;

        public CachedObject(U data) {
            this.data = data;
            lastUpdate = System.currentTimeMillis();
        }

        public U getData() {
            return data;
        }

        public void setData(U data) {
            this.data = data;
            lastUpdate = System.currentTimeMillis();
        }

        public long getLastUpdate() {
            return lastUpdate;
        }

        public long getAge() {
            return System.currentTimeMillis() - lastUpdate;
        }
    }
}

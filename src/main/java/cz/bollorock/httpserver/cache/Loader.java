/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver.cache;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public interface Loader<K, V> {

    public V load(K key) throws Exception;

    public long getLastModification(K key) throws Exception;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import io.netty.handler.codec.http.HttpResponseStatus;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import io.netty.util.CharsetUtil;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class ErrorResponseFactory {

    public static DefaultFullHttpResponse sendError(HttpResponseStatus errorStatus, String msg) {
        byte[] bytes
                = ("<html>\n"
                + "\t<head>\n"
                + "\t\t<title>Error: " + errorStatus.code() + "</title>\n"
                + "\t</head>\n"
                + "\t<body>\n"
                + "\t\t<h1>Failure: " + errorStatus + "</h1>\n"
                + "\t\t<code>" + msg + "</code>\n"
                + "\t</body>\n"
                + "</html>").getBytes(CharsetUtil.UTF_8);
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, errorStatus,
                Unpooled.copiedBuffer(bytes));
        response.headers().set(CONTENT_LENGTH, bytes.length);
        response.headers().set(CONTENT_TYPE, "text/html; charset=UTF-8");
        return response;
    }
}

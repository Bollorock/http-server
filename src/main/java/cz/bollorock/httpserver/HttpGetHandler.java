/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import cz.bollorock.httpserver.cache.FileLoader;
import cz.bollorock.httpserver.cache.LoadingCache;
import cz.bollorock.httpserver.security.SecurityUtils;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class HttpGetHandler implements HttpRequestHandler {

    private static final Logger LOG = Logger.getLogger(HttpGetHandler.class.getName());
    private static final MimetypesFileTypeMap MIME_TYPES = new MimetypesFileTypeMap();

    private final LoadingCache<String, byte[]> fileCache; //TODO remove guava cache
    private final SecurityUtils security;

    private final String webroot;

    public HttpGetHandler(String webRoot) throws IOException {
        this.webroot = new File(webRoot).getCanonicalPath();
        fileCache = new LoadingCache<>(new FileLoader());
        security = new SecurityUtils();
    }

    @Override
    public DefaultFullHttpResponse handle(FullHttpRequest request) {
        Logger.getLogger(HttpGetHandler.class.getName()).log(Level.INFO, "Received GET request: {0}", request.getUri());
        DefaultFullHttpResponse response;

        String uri;
        try {
            uri = URLDecoder.decode(request.getUri(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return ErrorResponseFactory.sendError(INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
        String path = uri.replaceAll("\\?.*$", "");
        if (path.endsWith("/")) {
            path += "/index.html";
        }
        File file = new File(webroot, path);
        try {
            path = file.getCanonicalPath();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return ErrorResponseFactory.sendError(INTERNAL_SERVER_ERROR, "");
        }
        if (!path.startsWith(webroot)) {
            return ErrorResponseFactory.sendError(FORBIDDEN, "");
        }
        if (file.isHidden() || !file.exists() || !file.canRead()) {
            return ErrorResponseFactory.sendError(NOT_FOUND, "File \"" + path + "\" wasn't found.");
        }
        if (!file.isFile()) {
            return ErrorResponseFactory.sendError(FORBIDDEN, "");
        }
        if (security.isProtected(path)) {
            String auth = request.headers().get(HttpHeaders.Names.AUTHORIZATION);
            if (!security.isValidAuth(file.getPath(), auth)) {
                response = ErrorResponseFactory.sendError(UNAUTHORIZED, "Incorect credentials");
                response.headers().set(HttpHeaders.Names.WWW_AUTHENTICATE, "Basic realm=\"Pozor zabezpečení\"");
                return response;
            }
        }

        try {
            byte[] bytes = fileCache.get(path);
            response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.copiedBuffer(bytes));
            response.headers().add(CONTENT_LENGTH, bytes.length);
            response.headers().set(CONTENT_TYPE, MIME_TYPES.getContentType(path));
            if (HttpHeaders.isKeepAlive(request)) {
                response.headers().add(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
            }
        } catch (Exception ex) {
            Logger.getLogger(HttpGetHandler.class.getName()).log(Level.SEVERE, null, ex);
            response = ErrorResponseFactory.sendError(INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
        return response;
    }

}

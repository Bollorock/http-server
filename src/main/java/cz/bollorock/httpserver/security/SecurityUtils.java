/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver.security;

import io.netty.util.CharsetUtil;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class SecurityUtils {

    private static final Logger LOGGER = Logger.getLogger(SecurityUtils.class.getName());

    public SecurityUtils() {

    }

    public boolean isProtected(String path) { //TODO improve isProtected
        return getAccessFile(path) != null;
    }

    public boolean isValidAuth(String path, String auth) {
        File accessFile = getAccessFile(path);
        if (accessFile == null) {
            return true;
        }
        if (auth == null || auth.length() < 6 || !auth.startsWith("Basic ")) {
            return false;
        }
        try {
            String[] data = new String(Base64.getDecoder().decode(auth.split(" ")[1]), "UTF-8").split(":");
            String userString = getUserPasswordString(data[0], data[1]);
            Optional<String> match = Files.lines(accessFile.toPath())
                    .filter((line) -> (userString.equals(line)))
                    .findAny();
            return match.isPresent();
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private String getUserPasswordString(String user, String password) {
        return user.trim() + ":" + hashPassword(password);
    }

    private String hashPassword(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] bytes = md.digest(password.getBytes(CharsetUtil.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return generatedPassword;
    }

    private File getAccessFile(String path) {
        File f = new File(path);
        File dir = f.getParentFile();
        File accessFile = new File(dir, ".htaccess");
        if (accessFile.canRead()) {
            return accessFile;
        } else {
            accessFile = new File(dir, "htaccess");
            return accessFile.canRead() ? accessFile : null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class HttpServerTest {

    private static HttpServer httpServer;
    private static HttpClient client;

    public HttpServerTest() {
    }

    @BeforeClass
    public static void init() throws IOException {
        System.out.println("Starting server");
        httpServer = new HttpServer(8080, "htdocs");
        httpServer.start();
        client = HttpClientBuilder.create().build();
    }

    @AfterClass
    public static void shutdown() throws IOException {
        System.out.println("Stopping server");
        httpServer.stop();
    }

    @Test
    public void test01() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("test01");
        HttpGet request = new HttpGet("http://localhost:8080/");
        HttpResponse response = client.execute(request);
        EntityUtils.consume(response.getEntity());
        assertEquals(HttpResponseStatus.OK.code(), response.getStatusLine().getStatusCode());
    }

    @Test
    public void test02() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("test02");
        HttpGet request = new HttpGet("http://localhost:8080/index.html");
        HttpResponse response = client.execute(request);
        EntityUtils.consume(response.getEntity());
        assertEquals(HttpResponseStatus.OK.code(), response.getStatusLine().getStatusCode());
    }

    @Test
    public void test03() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("test03");
        HttpGet request = new HttpGet("http://localhost:8080/secure/page.html");
        HttpResponse response = client.execute(request);
        EntityUtils.consume(response.getEntity());
        assertEquals(HttpResponseStatus.UNAUTHORIZED.code(), response.getStatusLine().getStatusCode());
    }

    @Test
    public void test04() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("test04");
        HttpGet request = new HttpGet("http://localhost:8080/secure/page.html");
        request.addHeader(HttpHeaders.Names.AUTHORIZATION, "Basic YWRtaW46YWRtaW4=");
        HttpResponse response = client.execute(request);
        EntityUtils.consume(response.getEntity());
        assertEquals(HttpResponseStatus.OK.code(), response.getStatusLine().getStatusCode());
    }

    @Test
    public void test05() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("test05");
        HttpGet request = new HttpGet("http://localhost:8080/secure/page.html");
        request.addHeader(HttpHeaders.Names.AUTHORIZATION, "Basic YWRtaW46c3BhdG5laGVzbG8=");
        System.out.println(request);
        HttpResponse response = client.execute(request);
        EntityUtils.consume(response.getEntity());
        assertEquals(HttpResponseStatus.UNAUTHORIZED.code(), response.getStatusLine().getStatusCode());
    }
}

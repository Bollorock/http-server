/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.httpserver;

import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class HttpGetHandlerTest {

    private static final String WEB_ROOT = "htdocs";

    @Test
    public void testHandle01() throws IOException {
        System.out.println("handle01");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.OK, result.getStatus());
    }

    @Test
    public void testHandle02() throws IOException {
        System.out.println("handle02");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/index.html");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.OK, result.getStatus());
    }

    @Test
    public void testHandle03() throws IOException {
        System.out.println("handle03");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/secure/page.html");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.UNAUTHORIZED, result.getStatus());
        assertTrue(result.headers().contains(HttpHeaders.Names.WWW_AUTHENTICATE));
    }

    @Test
    public void testHandle04() throws IOException {
        System.out.println("handle04");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/secure/page.html");
        request.headers().add(HttpHeaders.Names.AUTHORIZATION, "Basic YWRtaW46YWRtaW4=");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.OK, result.getStatus());
    }

    @Test
    public void testHandle05() throws IOException {
        System.out.println("handle05");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/nonexistent.html");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.NOT_FOUND, result.getStatus());
    }

    @Test
    public void testHandle06() throws IOException {
        System.out.println("handle06");
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/../nonexistent.html");
        HttpGetHandler instance = new HttpGetHandler(WEB_ROOT);
        DefaultFullHttpResponse result = instance.handle(request);

        assertEquals(HttpVersion.HTTP_1_1, result.getProtocolVersion());
        assertEquals(HttpResponseStatus.FORBIDDEN, result.getStatus());
    }

}

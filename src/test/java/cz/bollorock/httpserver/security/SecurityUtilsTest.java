/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.bollorock.httpserver.security;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Ladislav Mejzlík <lmejzlik@gmail.com>
 */
public class SecurityUtilsTest {
    
    public SecurityUtilsTest() {
    }

    @Test
    public void testIsProtected1() throws IOException {
        System.out.println("isProtected");
        String path = new File("htdocs/index.html").getCanonicalPath();
        SecurityUtils instance = new SecurityUtils();
        boolean expResult = false;
        boolean result = instance.isProtected(path);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsProtected2() throws IOException {
        System.out.println("isProtected");
        String path = new File("htdocs/secure/page.html").getCanonicalPath();
        SecurityUtils instance = new SecurityUtils();
        boolean expResult = true;
        boolean result = instance.isProtected(path);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsValidAuth1() throws IOException {
        System.out.println("isValidAuth");
        String path = new File("htdocs/secure/page.html").getCanonicalPath();
        String auth = "Basic YWRtaW46YWRtaW4=";
        SecurityUtils instance = new SecurityUtils();
        boolean expResult = true;
        boolean result = instance.isValidAuth(path, auth);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsValidAuth2() throws IOException {
        System.out.println("isValidAuth");
        String path = new File("htdocs/secure/page.html").getCanonicalPath();
        String auth = "Basic YWRtaW46c3BhdG5laGVzbG8=";
        SecurityUtils instance = new SecurityUtils();
        boolean expResult = false;
        boolean result = instance.isValidAuth(path, auth);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsValidAuth3() throws IOException {
        System.out.println("isValidAuth");
        String path = new File("htdocs/index.html").getCanonicalPath();
        String auth = "";
        SecurityUtils instance = new SecurityUtils();
        boolean expResult = true;
        boolean result = instance.isValidAuth(path, auth);
        assertEquals(expResult, result);
    }
    
}
